# cloud

#### 介绍
微服务实现网关统一鉴权框架
已迁移至https://gitee.com/china-zhz/zbone-cloud.git

#### 软件架构
 - spring-boot 2.6.3
 - spring-cloud 2021.0.1
 - spring-cloud-alibaba 2021.0.1.0
 - hutool 5.8.5
 - sa-token 1.30.0 
#### 项目结构
- cloud-common      公共工具类、枚举、常量等
- cloud-config      公共配置
- cloud-gateway     网关服务
- cloud-server-auth 授权服务
    - auth-api      授权服务对外暴露的feign接口
    - auth-web      授权服务web
- cloud-server-order 订单服务
- cloud-token       token模块


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx

#### 修改记录
##### 2022-11-9（dev-redis分支）
1、修改登陆接口为post
2、代码中的示例用户及权限修改为数据库控制
3、添加自定义代码生成器依赖
4、引入mybatis-plus作为持久层框架
5、添加sql脚本文件
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
