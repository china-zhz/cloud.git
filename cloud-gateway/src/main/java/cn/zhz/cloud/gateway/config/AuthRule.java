package cn.zhz.cloud.gateway.config;

import lombok.Data;

import java.util.List;

/**
 * @author ZHZ
 * @since 2023-02-03
 * 自定义过滤器配置类-接收配置文件中的属性
 */
@Data
public class AuthRule {

    private List<Rule> rules;

    private List<String> ignoreUrls;

    @Data
    public static class Rule {

        private String[] patterns;

        private String permissionCode;
    }
}
