package cn.zhz.cloud.gateway.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Service;

import java.util.List;

import static cn.zhz.cloud.token.constant.TokenConstant.SA_MODEL_KEY_PERMISSION;
import static cn.zhz.cloud.token.constant.TokenConstant.SA_MODEL_KEY_ROLE;

/**
 * 自定义权限验证接口扩展
 */
@Service
public class StpInterfaceImpl implements StpInterface {

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {

        return StpUtil.getTokenSession().getModel(SA_MODEL_KEY_PERMISSION, List.class);
    }

    /**
     * 返回一个账号所拥有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return StpUtil.getTokenSession().getModel(SA_MODEL_KEY_ROLE, List.class);
    }

}
