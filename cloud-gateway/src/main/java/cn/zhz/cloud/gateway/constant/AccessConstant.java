package cn.zhz.cloud.gateway.constant;

public class AccessConstant {


    public static final String ACCESS_HEADER_KEY = "Authorization";

    public static final String ACCESS_HEADER_VALUE_PREFIX = "Bearer ";
}
