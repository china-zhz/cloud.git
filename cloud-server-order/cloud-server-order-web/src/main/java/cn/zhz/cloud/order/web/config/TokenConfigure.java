package cn.zhz.cloud.order.web.config;

import cn.zhz.cloud.token.interceptor.HandlerTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * [Token 权限认证] 配置类
 *
 * @author zhz
 */
@Configuration
public class TokenConfigure implements WebMvcConfigurer {

    /**
     * 注册Token的拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HandlerTokenInterceptor())
                .addPathPatterns("/**")
                ;
    }


}
