package cn.zhz.cloud.order.web;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.env.Environment;

@Slf4j
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.zhz.cloud.*.api.client"})
@SpringBootApplication(scanBasePackages = {"cn.zhz.cloud"})
@MapperScan(basePackages = {"cn.zhz.cloud.order.web.mapper"})
public class CloudOrderServer {
    public static void main(String[] args) {
        Environment env = new SpringApplication(CloudOrderServer.class)
                .run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("" +
                        "\n----------------------------------------------------------\n" +
                        "\tApplication '{}' is running! Access URLs:\n" +
                        "\tLocal: \t\t\t{}://localhost:{}\n" +
                        "\tContext-Path: \t{}\n" +
                        "\tProfile(s): \t{}" +
                        "\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path"),
                env.getActiveProfiles());
    }
}
