package cn.zhz.cloud.order.web.controller;

import cn.zhz.cloud.auth.api.client.IAuthFeignApi;
import cn.zhz.cloud.auth.api.model.dto.TestFeignDto;
import cn.zhz.cloud.common.model.result.R;
import cn.zhz.cloud.token.dto.CurrentUserDto;
import cn.zhz.cloud.token.util.CurrentUserUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@Slf4j
@Tag(name = "订单接口")
public class OrderController {


    @Autowired
    private IAuthFeignApi iAuthFeignApi;

    @GetMapping("/order/info")
    @Operation(summary = "订单信息")
    public R<String> info() {
        return R.ok(CurrentUserUtil.getNickName() + "的订单信息");
    }

    @GetMapping("/userInfo")
    @Operation(summary = "用户信息")
    public R<CurrentUserDto> userInfo() {
        return R.ok(CurrentUserUtil.get());
    }

    @GetMapping("/order/auth/feign")
    public R<TestFeignDto> authFeign() {
        return iAuthFeignApi.feign();
    }
}
