package cn.zhz.cloud.common.config.web;


import cn.zhz.cloud.common.model.exception.BaseException;
import cn.zhz.cloud.common.model.exception.BusinessException;
import cn.zhz.cloud.common.model.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ZHZ
 * @date 2021-02-28
 * @apiNote 全局异常处理
 */

@Slf4j
@ControllerAdvice
public class GlobalControllerExceptionHandler {


    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public R<Void> handleBusinessException(BusinessException e) {
        log.error("请求异常结束，错误信息：", e);
        return R.custom(e.getCode(), e.getMessage(), null);
    }

    @ResponseBody
    @ExceptionHandler(BaseException.class)
    public R<Void> handleBusinessException(BaseException e) {
        log.error("请求异常结束，错误信息：", e);
        return R.custom(e.getCode(), e.getMessage(), null);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public R<Void> handleException(Exception e) {
        log.error("请求异常结束，错误信息：", e);
        return R.error(e.getMessage());
    }
}
