package cn.zhz.cloud.common.config.mybatis;

import cn.zhz.cloud.common.model.constant.SqlConstant;
import cn.zhz.cloud.token.util.CurrentUserUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author ZHZ
 * @since 2022-11-09
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.debug("start insert fill ....");
        this.strictInsertFill(metaObject, SqlConstant.CREATED_TIME, LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, SqlConstant.CREATED_BY, Long.class, CurrentUserUtil.getId());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.debug("start update fill ....");
        this.strictUpdateFill(metaObject, SqlConstant.UPDATED_TIME, LocalDateTime.class, LocalDateTime.now());
        this.strictUpdateFill(metaObject, SqlConstant.UPDATED_BY, Long.class, CurrentUserUtil.getId());

    }
}
