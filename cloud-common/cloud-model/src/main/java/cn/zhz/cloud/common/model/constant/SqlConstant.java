package cn.zhz.cloud.common.model.constant;

/**
 * @author ZHZ
 * @since 2022-11-09
 */
public class SqlConstant {

    /**
     * limit 1
     */
    public static final String LIMIT_1 = "limit 1";

    /*公共字段--start*/
    /**
     * 创建人
     */
    public static final String CREATED_BY = "createdBy";

    /**
     * 创建时间
     */
    public static final String CREATED_TIME = "createdTime";

    /**
     * 更新人
     */
    public static final String UPDATED_BY = "updatedBy";

    /**
     * 更新时间
     */
    public static final String UPDATED_TIME = "updatedTime";

    /**
     * 删除标识
     */
    public static final String DEL_FLAG = "delFlag";
    /*公共字段--end*/
}
