package cn.zhz.cloud.common.model.dto;

import lombok.Data;

/**
 * @author ZHZ
 * @since 2023-02-08
 */
@Data
public class PageParam {

    /**
     * 页面大小
     */
    private long size = 10;

    /**
     * 当前页
     */
    private long current = 1;

}
