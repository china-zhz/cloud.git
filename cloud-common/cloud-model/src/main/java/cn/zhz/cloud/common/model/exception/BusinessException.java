package cn.zhz.cloud.common.model.exception;


import cn.zhz.cloud.common.model.result.ResultCode;

/**
 * @author ZHZ
 * @date 2022-04-02
 * @apiNote 业务异常
 */
public class BusinessException extends BaseException {


    public BusinessException(String message) {
        super(ResultCode.FAIL.getCode(), message);
    }

    public BusinessException() {
        super(ResultCode.FAIL.getCode(), ResultCode.FAIL.getMessage());
    }

    public BusinessException(Integer code, String message) {
        super(code, message);
    }
}
