package cn.zhz.cloud.common.model.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final R<Void> R_OK = R.ok(null);

    private static final R<Void> R_FAILED = R.failed(ResultCode.FAIL.getMessage());

    private static final R<Void> R_ERROR = R.error(ResultCode.INTERNAL_SERVER_ERROR.getMessage());

    private Integer code;

    private String msg;

    private T data;

    public static R<Void> ok() {
        return R_OK;
    }

    public static <T> R<T> ok(T data) {
        return ok(ResultCode.SUCCESS.getMessage(), data);
    }

    public static <T> R<T> ok(String msg, T data) {
        return ok(ResultCode.SUCCESS, msg, data);
    }

    public static <T> R<T> ok(ResultCode resultCode, String msg, T data) {
        return restResult(resultCode.getCode(), msg, data);
    }

    public static <T> R<Void> failed() {
        return R_FAILED;
    }

    public static <T> R<T> failed(String msg) {
        return failed(ResultCode.FAIL, msg, null);
    }

    public static <T> R<T> failed(String msg, T data) {
        return failed(ResultCode.FAIL, msg, data);
    }

    public static <T> R<T> failed(ResultCode resultCode, String msg, T data) {
        return restResult(resultCode.getCode(), msg, data);
    }

    public static <T> R<Void> error() {
        return R_ERROR;
    }

    public static <T> R<T> error(String msg) {
        return restResult(ResultCode.INTERNAL_SERVER_ERROR.getCode(), msg, null);
    }

    public static <T> R<T> custom(Integer code, String msg, T data) {
        return restResult(code, msg, data);
    }

    public static <T> R<T> custom(ResultCode resultCode, T data) {
        return custom(resultCode.getCode(), resultCode.getMessage(), data);
    }


    private static <T> R<T> restResult(Integer code, String msg, T data) {
        return new R<T>(code, msg, data);
    }


}
