package cn.zhz.cloud.common.model.exception;

import cn.zhz.cloud.common.model.result.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZHZ
 * @date 2022-04-02
 * @apiNote 基类异常
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseException extends RuntimeException {

    private Integer code;

    public BaseException() {

        super(ResultCode.INTERNAL_SERVER_ERROR.getMessage());
        this.code = ResultCode.INTERNAL_SERVER_ERROR.getCode();

    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Integer code, String message) {
        super(message);
        this.code = code;
    }

}
