package cn.zhz.cloud.common.model.result;

/**
 * @author ZHZ
 * @date 2021-03-02
 * @apiNote Result代码常量
 */
public enum ResultCode {
    /**
     * 稍后继续尝试
     */
    CONTINUE(100, "稍后继续尝试"),
    /**
     * 成功
     */
    SUCCESS(200, "成功"),
    /**
     * 业务逻辑失败
     */
    FAIL(400, "失败"),
    /**
     * 未登录认证
     */
    UNAUTHORIZED(401, "未登录认证"),
    /**
     * 没有权限
     */
    FORBIDDEN(403, "没有权限"),
    /**
     * 接口不存在
     */
    NOT_FOUND(404, "接口不存在"),
    /**
     * 限流请求
     */
    TOO_MANY_REQUESTS(429, "请求太多了"),
    /**
     * 服务器内部错误
     */
    INTERNAL_SERVER_ERROR(500, "服务器内部错误"),
    /**
     * 服务暂时不可用
     */
    SERVICE_UNAVAILABLE(503, "服务暂时不可用"),


    ;

    private final Integer code;

    private final String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
