package cn.zhz.cloud.token.exception;


import cn.zhz.cloud.common.model.exception.BaseException;
import cn.zhz.cloud.common.model.result.ResultCode;

/**
 * @author ZHZ
 * @date 2022-04-02
 * @apiNote 令牌异常
 */
public class TokenException extends BaseException {


    public TokenException(String message) {
        super(ResultCode.FORBIDDEN.getCode(), message);
    }

    public TokenException() {
        super(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage());
    }
}
