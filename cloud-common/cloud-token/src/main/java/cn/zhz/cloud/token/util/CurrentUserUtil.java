package cn.zhz.cloud.token.util;

import cn.zhz.cloud.token.dto.CurrentUserDto;

/**
 * @author ZHZ
 * @date 2022-08-17
 * @apiNote 当前登录人工具类
 */
public class CurrentUserUtil {

    /**
     * 获取当前登陆人信息
     *
     * @return
     */
    public static CurrentUserDto get() {
        return CurrentUserContextHandler.getUser();
    }

    /**
     * 获取当前登录人-用户token
     *
     * @return
     */
    public static String getUserToken() {
        return CurrentUserContextHandler.getUserToken();
    }

    /**
     * 获取当前登录人-id
     *
     * @return
     */
    public static Long getId() {
        return get().getId();
    }

    /**
     * 获取当前登录人-用户名
     *
     * @return
     */
    public static String getUsername() {
        return get().getUsername();
    }

    /**
     * 获取当前登录人-真实姓名
     *
     * @return
     */
    public static String getNickName() {
        return get().getNickName();
    }

    /**
     * 获取当前登录人-头像
     *
     * @return
     */
    public static String getHeadImgUrl() {
        return get().getHeadImgUrl();
    }


}
