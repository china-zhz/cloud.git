package cn.zhz.cloud.token.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrentUserDto {

    /**
     * 用户id
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户真实姓名
     */
    private String nickName;
    /**
     * 用户性别 0：未设置；1：男；2：女
     */
    private Integer sex;
    /**
     * 用户头像
     */
    private String headImgUrl;

}
