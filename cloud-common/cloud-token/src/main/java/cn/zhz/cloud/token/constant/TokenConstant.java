package cn.zhz.cloud.token.constant;

import cn.zhz.cloud.token.dto.CurrentUserDto;

/**
 * @author ZHZ
 * @date 2022-08-17
 * @apiNote
 */
public class TokenConstant {

    /**
     * 往资源服务下传解析后token的请求头key
     */
    public static final String HEADER_USER = "user";
    /**
     * sa-token 存储model用户信息key
     */
    public static final String SA_MODEL_KEY_USER = "u";
    /**
     * sa-token 存储model权限信息key
     */
    public static final String SA_MODEL_KEY_PERMISSION = "p";
    /**
     * sa-token 存储model角色信息key
     */
    public static final String SA_MODEL_KEY_ROLE = "r";
    /**
     * AES加解密token的秘钥
     */
    public static final String TOKEN_AES_CRYPTO_KEY = "rfQt2J9F8oCw5QjTTNCVAQ==";
    /**
     * 匿名用户加密值 {"id":-1,"username":"匿名用户"}
     */
    public static final String ANONYMOUS_USER_ENCRYPT = "c6BCYOzNXUktNrI/XqnSZ+2U5LyXkw4qzgexjv9YveM=";
    /**
     * 匿名用户Dto
     */
    public static final CurrentUserDto ANONYMOUS_USER_DTO = CurrentUserDto.builder().id(-1L).username("匿名用户").build();
    /**
     * AES加解密token的秘钥-byte格式
     */
    public static final byte[] TOKEN_AES_CRYPTO_KEY_BYTE = {-83, -12, 45, -40, -97, 69, -14, -128, -80, -27, 8, -45, 76, -48, -107, 1};
}
