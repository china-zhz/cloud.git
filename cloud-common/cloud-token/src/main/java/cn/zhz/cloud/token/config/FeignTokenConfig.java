package cn.zhz.cloud.token.config;

import cn.zhz.cloud.token.util.CurrentUserUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

import static cn.zhz.cloud.token.constant.TokenConstant.HEADER_USER;

/**
 * @author ZHZ
 * @date 2022-04-04
 * @apiNote
 */
@Configuration
@ConditionalOnClass({RequestInterceptor.class, RequestTemplate.class})
public class FeignTokenConfig implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(HEADER_USER, CurrentUserUtil.getUserToken());
    }

}
