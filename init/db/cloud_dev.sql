/*
 Navicat Premium Data Transfer

 Source Server         : 本地库-mysql
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : 127.0.0.1:3306
 Source Schema         : cloud_dev

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 24/03/2023 09:23:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

CREATE database if NOT EXISTS `cloud_dev` default character set utf8mb4 collate utf8mb4_unicode_ci;
use `cloud_dev`;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` bigint UNSIGNED NOT NULL COMMENT '主键',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限名称',
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态（0：启用）',
  `created_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '删除标识（1970-01-01 00:00:00：未删除；其他：已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'auth-user-info', '用户信息', NULL, 0, NULL, '2022-11-09 12:54:29', NULL, NULL, '1970-01-01 00:00:00');
INSERT INTO `auth_permission` VALUES (2, 'auth-user-save', '用户保存', NULL, 0, NULL, '2022-11-09 12:55:16', NULL, NULL, '1970-01-01 00:00:00');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role`  (
  `id` bigint UNSIGNED NOT NULL COMMENT '主键',
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色编码',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态（0：启用）',
  `created_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '删除标识（1970-01-01 00:00:00：未删除；其他：已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES (1, 'admin', '管理员', NULL, 0, NULL, '2022-11-09 12:56:41', NULL, NULL, '1970-01-01 00:00:00');

-- ----------------------------
-- Table structure for auth_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_permission`;
CREATE TABLE `auth_role_permission`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色id',
  `permission_id` bigint UNSIGNED NOT NULL COMMENT '权限id',
  `created_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '删除标识（1970-01-01 00:00:00：未删除；其他：已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色、权限关联关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_role_permission
-- ----------------------------
INSERT INTO `auth_role_permission` VALUES (1, 1, 1, NULL, '2022-11-09 12:55:38', NULL, NULL, '1970-01-01 00:00:00');
INSERT INTO `auth_role_permission` VALUES (2, 1, 2, NULL, '2022-11-09 12:55:48', NULL, NULL, '1970-01-01 00:00:00');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` bigint UNSIGNED NOT NULL COMMENT '主键',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `sex` tinyint(1) NOT NULL DEFAULT 0 COMMENT '性别（0：无；1：男；2：女）',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态（0：启用）',
  `created_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '删除标识（1970-01-01 00:00:00：未删除；其他：已删除)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_uni_username`(`username` ASC, `del_flag` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES (1590182261186850818, 'zhangsan', 'vcOUE8JTIRwGN5fF4OPkKA==', '张三', 1, NULL, NULL, 0, NULL, '2022-11-09 11:19:30', NULL, '2023-02-08 17:21:12', '1970-01-01 00:00:00');
INSERT INTO `auth_user` VALUES (1623250800713846785, 'lisi', 'VBQX0HZDq2OapvZfzAtwjA==', '李四', 1, NULL, NULL, 0, NULL, '2023-02-08 17:22:04', 1590182261186850818, '2023-02-08 17:48:00', '1970-01-01 00:00:00');
INSERT INTO `auth_user` VALUES (1623256974255357953, 'wangwu', 'ly5vrWSfZpaACC8s2B43wg==', '王五', 2, NULL, NULL, 0, 1590182261186850818, '2023-02-08 17:46:36', 1590182261186850818, '2023-02-08 17:47:46', '1970-01-01 00:00:00');

-- ----------------------------
-- Table structure for auth_user_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_role`;
CREATE TABLE `auth_user_role`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户id',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色id',
  `created_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '删除标识（1970-01-01 00:00:00：未删除；其他：已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户、角色关联关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth_user_role
-- ----------------------------
INSERT INTO `auth_user_role` VALUES (1, 1590182261186850818, 1, NULL, '2022-11-09 12:56:56', NULL, NULL, '1970-01-01 00:00:00');
INSERT INTO `auth_user_role` VALUES (2, 1623256974255357953, 1, NULL, NULL, NULL, NULL, '1970-01-01 00:00:00');
INSERT INTO `auth_user_role` VALUES (3, 1623250800713846785, 1, NULL, NULL, NULL, NULL, '1970-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
