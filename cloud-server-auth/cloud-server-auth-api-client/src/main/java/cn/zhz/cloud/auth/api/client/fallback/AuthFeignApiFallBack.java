package cn.zhz.cloud.auth.api.client.fallback;

import cn.zhz.cloud.auth.api.client.IAuthFeignApi;
import cn.zhz.cloud.auth.api.model.dto.TestFeignDto;
import cn.zhz.cloud.common.model.result.R;
import cn.zhz.cloud.common.model.result.ResultCode;
import org.springframework.stereotype.Component;

/**
 * @author ZHZ
 * @since 2023-02-07
 */
@Component
public class AuthFeignApiFallBack implements IAuthFeignApi {
    /**
     * feign测试
     *
     * @return
     */
    @Override
    public R<TestFeignDto> feign() {
        return R.custom(ResultCode.SERVICE_UNAVAILABLE, null);
    }
}
