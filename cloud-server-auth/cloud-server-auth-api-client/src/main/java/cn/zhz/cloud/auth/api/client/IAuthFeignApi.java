package cn.zhz.cloud.auth.api.client;

import cn.zhz.cloud.auth.api.client.fallback.AuthFeignApiFallBack;
import cn.zhz.cloud.auth.api.model.dto.TestFeignDto;
import cn.zhz.cloud.common.model.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author ZHZ
 * @date 2022-08-18
 * @apiNote
 */
@FeignClient(value = "cloud-server-auth", fallback = AuthFeignApiFallBack.class)
public interface IAuthFeignApi {

    /**
     * feign测试
     *
     * @return
     */
    @GetMapping("/auth/feign")
    R<TestFeignDto> feign();
}
