package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.dto.RoleQueryDto;
import cn.zhz.cloud.auth.web.model.dto.RoleSaveDto;
import cn.zhz.cloud.auth.web.model.po.RolePo;
import cn.zhz.cloud.auth.web.model.vo.RoleVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色 Service接口
 *
 * @author zhz
 * @since 2022-11-08
 */
public interface IRoleService extends IService<RolePo> {

    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    Page<RoleVo> page(RoleQueryDto queryDto);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    RoleVo info(Long id);

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    Long save(RoleSaveDto saveDto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

    /**
     * 获取角色编码列表
     *
     * @param userId
     * @return
     */
    List<String> getRoleCodeList(Long userId);
}
