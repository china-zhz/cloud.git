package cn.zhz.cloud.auth.web.controller;

import cn.zhz.cloud.auth.web.model.dto.UserQueryDto;
import cn.zhz.cloud.auth.web.model.dto.UserSaveDto;
import cn.zhz.cloud.auth.web.model.vo.UserVo;
import cn.zhz.cloud.auth.web.service.IUserService;
import cn.zhz.cloud.common.model.result.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 用户 Controller
 *
 * @author zhz
 * @since 2022-11-08
 */
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Tag(name = "用户 接口集合")
public class UserController {

    private final IUserService iUserService;


    @Operation(summary = "分页查询")
    @GetMapping(value = "/page")
    public R<Page<UserVo>> page(UserQueryDto queryDto) {
        return R.ok(iUserService.page(queryDto));
    }

    @Operation(summary = "详情")
    @GetMapping(value = "/info/{id}")
    public R<UserVo> info(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        return R.ok(iUserService.info(id));
    }

    @Operation(summary = "保存")
    @PostMapping(value = "/save")
    public R<String> save(@Valid @RequestBody UserSaveDto saveDto) {
        return R.ok(String.valueOf(iUserService.save(saveDto)));
    }

    @Operation(summary = "删除")
    @PostMapping(value = "/delete/{id}")
    public R<Void> delete(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        iUserService.delete(id);
        return R.ok();
    }

}
