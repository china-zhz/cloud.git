package cn.zhz.cloud.auth.web.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhz
 * @since 2022-11-08
 */
@Data
@Schema(name = "UserRoleVo 输出参数", description = " 输出参数")
public class UserRoleVo implements Serializable {

    private static final long serialVersionUID = 1L;


    @Schema(description = "主键")
    private Long id;

    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "角色id")
    private Long roleId;

}
