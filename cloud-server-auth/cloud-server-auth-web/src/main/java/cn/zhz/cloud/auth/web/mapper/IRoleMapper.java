package cn.zhz.cloud.auth.web.mapper;

import cn.zhz.cloud.auth.web.model.po.RolePo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 角色 Mapper接口
 * @author zhz
 * @since 2022-11-08
 */
public interface IRoleMapper extends BaseMapper<RolePo> {


    /**
     * 获取用户的角色编码列表
     *
     * @param userId
     * @return
     */
    List<String> getRoleCodeListByUserId(Long userId);
}
