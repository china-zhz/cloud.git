package cn.zhz.cloud.auth.web.model.po;

import cn.zhz.cloud.common.model.po.BasePo;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 权限
 *
 * @author zhz
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("auth_permission")
public class PermissionPo extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 权限编码
     */
    private String code;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 描述
     */
    private String details;

    /**
     * 状态（0：启用）
     */
    private Integer status;


}
