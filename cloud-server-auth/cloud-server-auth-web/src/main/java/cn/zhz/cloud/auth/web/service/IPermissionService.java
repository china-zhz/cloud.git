package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.po.PermissionPo;
import cn.zhz.cloud.auth.web.model.dto.PermissionQueryDto;
import cn.zhz.cloud.auth.web.model.dto.PermissionSaveDto;
import cn.zhz.cloud.auth.web.model.vo.PermissionVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * 权限 Service接口
 * @author zhz
 * @since 2022-11-08
 */
public interface IPermissionService extends IService<PermissionPo> {

    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    Page<PermissionVo> page(PermissionQueryDto queryDto);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    PermissionVo info(Long id);

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    Long save(PermissionSaveDto saveDto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

    /**
     * 获取权限编码列表
     *
     * @param userId
     * @return
     */
    List<String> getPermissionCodeList(Long userId);
}
