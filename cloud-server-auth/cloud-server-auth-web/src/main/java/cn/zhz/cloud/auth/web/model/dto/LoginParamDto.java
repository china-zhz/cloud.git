package cn.zhz.cloud.auth.web.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author ZHZ
 * @since 2022-11-08
 */
@Data
@Schema(name = "登录 参数", description = "登录 参数")
public class LoginParamDto {

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

}
