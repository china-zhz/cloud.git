package cn.zhz.cloud.auth.web.model.po;

import cn.zhz.cloud.common.model.po.BasePo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhz
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("auth_role_permission")
public class RolePermissionPo extends BasePo {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限id
     */
    private Long permissionId;


}
