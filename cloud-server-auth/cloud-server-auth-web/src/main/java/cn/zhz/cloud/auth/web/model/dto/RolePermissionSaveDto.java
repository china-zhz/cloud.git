package cn.zhz.cloud.auth.web.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhz
 * @since 2022-11-08
 */
@Data
@Schema(name = "RolePermissionSaveDto 保存参数", description = " 保存参数")
public class RolePermissionSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;


    @Schema(description = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Schema(description = "角色id")
    private Long roleId;

    @Schema(description = "权限id")
    private Long permissionId;

}
