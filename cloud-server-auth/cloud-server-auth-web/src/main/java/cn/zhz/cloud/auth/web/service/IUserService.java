package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.po.UserPo;
import cn.zhz.cloud.auth.web.model.dto.UserQueryDto;
import cn.zhz.cloud.auth.web.model.dto.UserSaveDto;
import cn.zhz.cloud.auth.web.model.vo.UserVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 用户 Service接口
 * @author zhz
 * @since 2022-11-08
 */
public interface IUserService extends IService<UserPo> {

    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    Page<UserVo> page(UserQueryDto queryDto);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    UserVo info(Long id);

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    Long save(UserSaveDto saveDto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

}
