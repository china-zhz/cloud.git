package cn.zhz.cloud.auth.web.model.dto;

import cn.zhz.cloud.common.model.dto.PageParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 权限
 *
 * @author zhz
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "PermissionQueryDto 查询参数", description = "权限 查询参数")
public class PermissionQueryDto extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;


    @Schema(description = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Schema(description = "权限编码")
    private String code;

    @Schema(description = "权限名称")
    private String name;

    @Schema(description = "描述")
    private String details;

    @Schema(description = "状态（0：启用）")
    private Integer status;

}
