package cn.zhz.cloud.auth.web.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.zhz.cloud.auth.api.model.dto.TestFeignDto;
import cn.zhz.cloud.auth.web.model.dto.LoginParamDto;
import cn.zhz.cloud.auth.web.service.IAuthService;
import cn.zhz.cloud.common.model.result.R;
import cn.zhz.cloud.token.dto.CurrentUserDto;
import cn.zhz.cloud.token.util.CurrentUserUtil;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class AuthController {

    private final IAuthService iAuthService;


    @Operation(summary = "登录")
    @PostMapping("/login")
    public R<String> login(@RequestBody LoginParamDto loginParamDto) {
        return R.ok(iAuthService.login(loginParamDto));
    }

    @Operation(summary = "注销登录")
    @PostMapping("/logout")
    public R<Void> logout() {
        StpUtil.logout();
        return R.ok();
    }

    @GetMapping("/info")
    public R<CurrentUserDto> info() {
        return R.ok(CurrentUserUtil.get());
    }


    @GetMapping("/feign")
    public R<TestFeignDto> feign() {
        TestFeignDto feignDto = new TestFeignDto();
        feignDto.setCode("feignCode");
        feignDto.setName(CurrentUserUtil.getNickName() + "的feign名称");
        feignDto.setContent("feign内容");
        return R.ok(feignDto);
    }

}
