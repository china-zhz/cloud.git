package cn.zhz.cloud.auth.web.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.zhz.cloud.auth.web.service.IPermissionService;
import cn.zhz.cloud.auth.web.service.IRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自定义权限验证接口扩展
 */

@Service
@RequiredArgsConstructor
public class StpInterfaceImpl implements StpInterface {

    private final IRoleService iRoleService;

    private final IPermissionService iPermissionService;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return iPermissionService.getPermissionCodeList(Long.valueOf(String.valueOf(loginId)));
    }

    /**
     * 返回一个账号所拥有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return iRoleService.getRoleCodeList(Long.valueOf(String.valueOf(loginId)));
    }

}

