package cn.zhz.cloud.auth.web.mapper;

import cn.zhz.cloud.auth.web.model.po.UserPo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户 Mapper接口
 *
 * @author zhz
 * @since 2022-11-08
 */
public interface IUserMapper extends BaseMapper<UserPo> {

}
