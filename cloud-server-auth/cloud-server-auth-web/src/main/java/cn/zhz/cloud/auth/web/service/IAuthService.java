package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.dto.LoginParamDto;

/**
 * @author ZHZ
 * @since 2022-11-08
 */
public interface IAuthService {

    /**
     * 登录
     *
     * @param loginParamDto
     * @return
     */
    String login(LoginParamDto loginParamDto);
}
