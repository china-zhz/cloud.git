package cn.zhz.cloud.auth.web.service.impl;

import cn.zhz.cloud.auth.web.model.po.RolePermissionPo;
import cn.zhz.cloud.auth.web.model.dto.RolePermissionQueryDto;
import cn.zhz.cloud.auth.web.model.dto.RolePermissionSaveDto;
import cn.zhz.cloud.auth.web.model.vo.RolePermissionVo;
import cn.zhz.cloud.auth.web.mapper.IRolePermissionMapper;
import cn.zhz.cloud.auth.web.service.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhz
 * @since 2022-11-08
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<IRolePermissionMapper, RolePermissionPo> implements IRolePermissionService {


    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    @Override
    public Page<RolePermissionVo> page(RolePermissionQueryDto queryDto) {
        long current = queryDto.getCurrent();
        long pageSize = queryDto.getSize();
        Page<RolePermissionPo> rolePermissionPoPage = baseMapper.selectPage(new Page<>(current, pageSize), Wrappers.lambdaQuery(RolePermissionPo.class));
        List<RolePermissionVo> rolePermissionVoList = rolePermissionPoPage.getRecords().stream().map(rolePermissionPo -> BeanUtil.copyProperties(rolePermissionPo, RolePermissionVo.class)).collect(Collectors.toList());

        return new Page<RolePermissionVo>(current, pageSize)
            .setTotal(rolePermissionPoPage.getTotal())
            .setRecords(rolePermissionVoList);
    }

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public RolePermissionVo info(Long id) {
        RolePermissionPo rolePermissionPo = baseMapper.selectById(id);
        if (rolePermissionPo == null) {
        // TODO
        }
        return BeanUtil.copyProperties(rolePermissionPo, RolePermissionVo.class);
    }

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    @Override
    public Long save(RolePermissionSaveDto saveDto) {
        RolePermissionPo rolePermissionPo = BeanUtil.copyProperties(saveDto, RolePermissionPo.class);
        this.saveOrUpdate(rolePermissionPo);
        return rolePermissionPo.getId();
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

}
