package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.po.RolePermissionPo;
import cn.zhz.cloud.auth.web.model.dto.RolePermissionQueryDto;
import cn.zhz.cloud.auth.web.model.dto.RolePermissionSaveDto;
import cn.zhz.cloud.auth.web.model.vo.RolePermissionVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author zhz
 * @since 2022-11-08
 */
public interface IRolePermissionService extends IService<RolePermissionPo> {

    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    Page<RolePermissionVo> page(RolePermissionQueryDto queryDto);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    RolePermissionVo info(Long id);

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    Long save(RolePermissionSaveDto saveDto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

}
