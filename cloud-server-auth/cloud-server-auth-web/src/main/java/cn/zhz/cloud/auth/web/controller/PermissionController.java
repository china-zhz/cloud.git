package cn.zhz.cloud.auth.web.controller;

import cn.zhz.cloud.auth.web.model.dto.PermissionQueryDto;
import cn.zhz.cloud.auth.web.model.dto.PermissionSaveDto;
import cn.zhz.cloud.auth.web.model.vo.PermissionVo;
import cn.zhz.cloud.auth.web.service.IPermissionService;
import cn.zhz.cloud.common.model.result.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 权限 Controller
 *
 * @author zhz
 * @since 2022-11-08
 */
@RestController
@RequestMapping("/permission")
@RequiredArgsConstructor
@Tag(name = "权限 接口集合")
public class PermissionController {

    private final IPermissionService iPermissionService;


    @Operation(summary = "分页查询")
    @GetMapping(value = "/page")
    public R<Page<PermissionVo>> page(PermissionQueryDto queryDto) {
        return R.ok(iPermissionService.page(queryDto));
    }

    @Operation(summary = "详情")
    @GetMapping(value = "/info/{id}")
    public R<PermissionVo> info(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        return R.ok(iPermissionService.info(id));
    }

    @Operation(summary = "保存")
    @PostMapping(value = "/save")
    public R<String> save(@Valid @RequestBody PermissionSaveDto saveDto) {
        return R.ok(String.valueOf(iPermissionService.save(saveDto)));
    }

    @Operation(summary = "删除")
    @PostMapping(value = "/delete/{id}")
    public R<Void> delete(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        iPermissionService.delete(id);
        return R.ok();
    }

}
