package cn.zhz.cloud.auth.web.constant;

import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.crypto.SecureUtil;

import java.util.Arrays;

/**
 * @author ZHZ
 * @date 2022-08-17
 * @apiNote
 */
public class AuthConstant {


    /**
     * AES加解密的秘钥
     */
    public static final String LOGIN_AES_CRYPTO_KEY = "u7bt0+wavTbE7pmZ/f00Og==";

    /**
     * AES加解密的秘钥-byte格式
     */
    public static final byte[] LOGIN_AES_CRYPTO_KEY_BYTE = {-69, -74, -19, -45, -20, 26, -67, 54, -60, -18, -103, -103, -3, -3, 52, 58};


    public static void main(String[] args) {
        SecureUtil.aes().getSecretKey();
        byte[] keyByte = SecureUtil.generateKey("AES").getEncoded();
        String ketStr = Base64Encoder.encode(keyByte);
        System.out.println(ketStr);
        System.out.println(Arrays.toString(keyByte));
    }
}
