package cn.zhz.cloud.auth.web.model.dto;

import cn.zhz.cloud.common.model.dto.PageParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户
 *
 * @author zhz
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "UserQueryDto 查询参数", description = "用户 查询参数")
public class UserQueryDto extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "真实姓名")
    private String nickName;

    @Schema(description = "性别（0：无；1：男；2：女）")
    private Integer sex;

    @Schema(description = "手机号")
    private String phone;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "状态（0：启用）")
    private Integer status;

}
