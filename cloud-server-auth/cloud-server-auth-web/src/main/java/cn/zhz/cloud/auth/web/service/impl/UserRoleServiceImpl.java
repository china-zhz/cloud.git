package cn.zhz.cloud.auth.web.service.impl;

import cn.zhz.cloud.auth.web.mapper.IUserRoleMapper;
import cn.zhz.cloud.auth.web.service.IUserRoleService;
import cn.zhz.cloud.auth.web.model.po.UserRolePo;
import cn.zhz.cloud.auth.web.model.dto.UserRoleQueryDto;
import cn.zhz.cloud.auth.web.model.dto.UserRoleSaveDto;
import cn.zhz.cloud.auth.web.model.vo.UserRoleVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhz
 * @since 2022-11-08
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<IUserRoleMapper, UserRolePo> implements IUserRoleService {


    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    @Override
    public Page<UserRoleVo> page(UserRoleQueryDto queryDto) {
        long current = queryDto.getCurrent();
        long pageSize = queryDto.getSize();
        Page<UserRolePo> userRolePoPage = baseMapper.selectPage(new Page<>(current, pageSize), Wrappers.lambdaQuery(UserRolePo.class));
        List<UserRoleVo> userRoleVoList = userRolePoPage.getRecords().stream().map(userRolePo -> BeanUtil.copyProperties(userRolePo, UserRoleVo.class)).collect(Collectors.toList());

        return new Page<UserRoleVo>(current, pageSize)
            .setTotal(userRolePoPage.getTotal())
            .setRecords(userRoleVoList);
    }

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public UserRoleVo info(Long id) {
        UserRolePo userRolePo = baseMapper.selectById(id);
        if (userRolePo == null) {
        // TODO
        }
        return BeanUtil.copyProperties(userRolePo, UserRoleVo.class);
    }

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    @Override
    public Long save(UserRoleSaveDto saveDto) {
        UserRolePo userRolePo = BeanUtil.copyProperties(saveDto, UserRolePo.class);
        this.saveOrUpdate(userRolePo);
        return userRolePo.getId();
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(Long id) {
        baseMapper.deleteById(id);
    }

}
