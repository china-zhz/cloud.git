package cn.zhz.cloud.auth.web.mapper;

import cn.zhz.cloud.auth.web.model.po.PermissionPo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 权限 Mapper接口
 * @author zhz
 * @since 2022-11-08
 */
public interface IPermissionMapper extends BaseMapper<PermissionPo> {


    /**
     * 获取用户的权限编码列表
     *
     * @param userId
     * @return
     */
    List<String> getPermissionCodeListByUserId(Long userId);
}
