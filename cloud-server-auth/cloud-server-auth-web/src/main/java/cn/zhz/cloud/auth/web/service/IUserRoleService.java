package cn.zhz.cloud.auth.web.service;

import cn.zhz.cloud.auth.web.model.po.UserRolePo;
import cn.zhz.cloud.auth.web.model.dto.UserRoleQueryDto;
import cn.zhz.cloud.auth.web.model.dto.UserRoleSaveDto;
import cn.zhz.cloud.auth.web.model.vo.UserRoleVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author zhz
 * @since 2022-11-08
 */
public interface IUserRoleService extends IService<UserRolePo> {

    /**
     * 分页查询
     *
     * @param queryDto 查询参数
     * @return
     */
    Page<UserRoleVo> page(UserRoleQueryDto queryDto);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    UserRoleVo info(Long id);

    /**
     * 保存
     *
     * @param saveDto 保存参数
     * @return 主键
     */
    Long save(UserRoleSaveDto saveDto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

}
