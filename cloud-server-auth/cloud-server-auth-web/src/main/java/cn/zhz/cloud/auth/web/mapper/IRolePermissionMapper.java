package cn.zhz.cloud.auth.web.mapper;

import cn.zhz.cloud.auth.web.model.po.RolePermissionPo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zhz
 * @since 2022-11-08
 */
public interface IRolePermissionMapper extends BaseMapper<RolePermissionPo> {

}
