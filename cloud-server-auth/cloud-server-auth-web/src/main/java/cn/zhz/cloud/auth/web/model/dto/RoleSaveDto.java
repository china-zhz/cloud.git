package cn.zhz.cloud.auth.web.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色
 *
 * @author zhz
 * @since 2022-11-08
 */
@Data
@Schema(name = "RoleSaveDto 保存参数", description = "角色 保存参数")
public class RoleSaveDto implements Serializable {

    private static final long serialVersionUID = 1L;


    @Schema(description = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Schema(description = "角色编码")
    private String code;

    @Schema(description = "角色名称")
    private String name;

    @Schema(description = "描述")
    private String details;

    @Schema(description = "状态（0：启用）")
    private Integer status;

}
