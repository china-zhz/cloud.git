package cn.zhz.cloud.auth.web.controller;

import cn.zhz.cloud.auth.web.model.dto.RoleQueryDto;
import cn.zhz.cloud.auth.web.model.dto.RoleSaveDto;
import cn.zhz.cloud.auth.web.model.vo.RoleVo;
import cn.zhz.cloud.auth.web.service.IRoleService;
import cn.zhz.cloud.common.model.result.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 角色 Controller
 *
 * @author zhz
 * @since 2022-11-08
 */
@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
@Tag(name = "角色 接口集合")
public class RoleController {

    private final IRoleService iRoleService;


    @Operation(summary = "分页查询")
    @GetMapping(value = "/page")
    public R<Page<RoleVo>> page(RoleQueryDto queryDto) {
        return R.ok(iRoleService.page(queryDto));
    }

    @Operation(summary = "详情")
    @GetMapping(value = "/info/{id}")
    public R<RoleVo> info(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        return R.ok(iRoleService.info(id));
    }

    @Operation(summary = "保存")
    @PostMapping(value = "/save")
    public R<String> save(@Valid @RequestBody RoleSaveDto saveDto) {
        return R.ok(String.valueOf(iRoleService.save(saveDto)));
    }

    @Operation(summary = "删除")
    @PostMapping(value = "/delete/{id}")
    public R<Void> delete(@Valid @NotNull(message = "id不能为空") @PathVariable("id") Long id) {
        iRoleService.delete(id);
        return R.ok();
    }

}
