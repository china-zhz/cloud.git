package cn.zhz.cloud.auth.web.model.po;

import cn.zhz.cloud.common.model.po.BasePo;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色
 *
 * @author zhz
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("auth_role")
public class RolePo extends BasePo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 描述
     */
    private String details;

    /**
     * 状态（0：启用）
     */
    private Integer status;


}
