package cn.zhz.cloud.auth.api.model.dto;

import lombok.Data;

/**
 * @author ZHZ
 * @date 2022-08-18
 * @apiNote
 */
@Data
public class TestFeignDto {

    private String code;

    private String name;

    private String content;
}
